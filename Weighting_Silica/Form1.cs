﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Weighting_Silica
{
    public partial class Form1 : Form
    {
        DBService dbs = new DBService();
        DbServiceSQL dbSql = new DbServiceSQL();
        string StrSql = $@"";
        public Silica currentSilicaL;
        public Silica currentSilicaR;
        public static int WorkerID = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Srl_Weighting.PortName = ConfigurationManager.AppSettings["COML"];
            Srl_WeightingR.PortName = ConfigurationManager.AppSettings["COMR"];

            try
            {
                //if (!this.Srl_Weighting.IsOpen) this.Srl_Weighting.Open();
                //if (!this.Srl_WeightingR.IsOpen) this.Srl_WeightingR.Open();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
                throw;
            }


        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Srl_Weighting.IsOpen) Srl_Weighting.Close();
            if (Srl_WeightingR.IsOpen) Srl_WeightingR.Close();
            Application.Exit();
        }

        private void Srl_Weighting_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(Txt_ItemR.Text) && !string.IsNullOrEmpty(Txt_MixR.Text))
            {
                var sp = (SerialPort)sender;

                // sp.Encoding = Encoding.GetEncoding("utf-8");
                var indata = sp.ReadExisting();

                var regex = new Regex(@"-?[0-9]*\.?[0-9]+");

                if (regex.IsMatch(indata))
                {
                    var weight = 0.0;
                    double.TryParse(regex.Match(indata).Value, out weight);
                    weight = Math.Abs(weight);
                    if (weight > 180)
                    {
                        Lbl_ActualWeight.Text = Math.Abs(weight).ToString();
                        currentSilicaL.ActualWeight = weight;
                        Weighting(currentSilicaL); // כאן תבוא הלוגיקה בהמשך
                        // להוסיף בדיקה האם נקרא המפרט קודם!
                    }
                }
            }
            else
            {
                MessageBox.Show("תערובת או מק'ט לא יכולים להיות ריקים");
            }
        }

        private void Srl_WeightingR_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

        }

        /// <summary>
        /// קריאת פתק ליווי גליל חדש
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Txt_Item_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyData == Keys.Enter)
            //{
            //    // איפוס תצוגת המשקלים
            //    Lbl_ActualWeight.Text = "";
            //    Lbl_ActualWeight.BackColor = Control.DefaultBackColor;
            //    try
            //    {
            //        // שליפת נתוני הסיליקה
            //        currentSilicaL = new Silica(Txt_ItemR.Text.Trim(), int.Parse(Txt_ItemR.Text.Split('-')[1].ToString()));
            //        currentSilicaL.GetSilicaData();
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message);
            //        throw;
            //    }
            //}
        }

        private void Txt_MixR_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void Txt_ItemR_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                // איפוס תצוגת המשקלים
                Lbl_ActualWeight.Text = "";
                Lbl_ActualWeight.BackColor = Control.DefaultBackColor;
                try
                {
                    // שליפת נתוני הסיליקה
                    currentSilicaR = new Silica(Txt_ItemR.Text.PadRight(21, ' ').Substring(0, 15), /*int.Parse(Txt_ItemR.Text.Split('-')[1].ToString())*/ Txt_ItemR.Text.PadRight(21, ' ').Substring(15, 6));
                    currentSilicaR.GetSilicaData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        public void Weighting(Silica S)
        {
            if (S.ActualWeight > 0)
            {
                Lbl_ActualWeight.Text = S.ActualWeight.ToString();

                if (S.IsWeightInTolerance())
                {
                    Lbl_ActualWeight.BackColor = Color.LightGreen;
                }
                else
                {
                    Lbl_ActualWeight.BackColor = Color.Red;
                }
                // חישוב סטיית המשקל
                Lbl_Dev.Text = S.CalculateDeviation().ToString() + " %";

                // הזנת נתונים לבסיס הנתונים
                DialogResult D = MessageBox.Show(string.Format($"האם לאשר משקל {S.ActualWeight} לפריט ? ", S.ActualWeight, S.Item, MessageBoxButtons.YesNo));
                if (D == DialogResult.OK)
                {
                    S.AddWeightToDB();
                    S.UpdateManufacture();

                    // איפוס נתונים והמתנה לשקילה הבאה
                    Txt_ItemR.Text = "";
                    Lbl_ActualWeight.Text = "";
                    Lbl_Dev.Text = "";
                    Txt_ItemR.Focus();
                }

            }
        }

        private void התחברותToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Btn_LogIn_Click(object sender, EventArgs e)
        {
            StrSql = $@"SELECT OVED,PRATI,FAMILY
                        FROM isufkv.isavl10
                        WHERE oved=00{Txt_WorkerNum.Text}";
            DataTable DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                WorkerID = int.Parse(DT.Rows[0]["OVED"].ToString());
                Lbl_Worker.Text = string.Format(".שלום {0}", DT.Rows[0]["prati"].ToString() + " " + DT.Rows[0]["family"].ToString());
                Txt_WorkerNum.Text = "";
                groupBox1.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random R = new Random(180);
            currentSilicaR.ActualWeight = R.Next(250);
            Weighting(currentSilicaR);
        }


    }
}
