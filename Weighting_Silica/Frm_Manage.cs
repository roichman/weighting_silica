﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Weighting_Silica
{
    public partial class Frm_Manage : Form
    {
        public Frm_Manage()
        {
            InitializeComponent();
        }

        private void Frm_Manage_Load(object sender, EventArgs e)
        {
            Txt_LeftCOM.Text = ConfigurationManager.AppSettings["COML"];
            Txt_RightCOM.Text = ConfigurationManager.AppSettings["COMR"];
        }

        private void Btn_Update_Click(object sender, EventArgs e)
        {
            ConfigurationManager.AppSettings["COML"] = Txt_LeftCOM.Text;
            ConfigurationManager.AppSettings["COMR"] = Txt_RightCOM.Text;
        }
    }
}
