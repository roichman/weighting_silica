﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Weighting_Silica
{
    public class Silica
    {
        string Lib_TAPIALI = "Tapidali";
        string Rzpali = "rzpadali";
        string Bpcs = "BpcsDali";
        public int ID { get; set; }
        public string Item { get; set; }
        public string MacID { get; set; }
        public int DepID { get; set; }
        public int WorkCenter { get; set; }
        public int Num { get; set; }
        public string MixNum { get; set; }
        public int MixID { get; set; }
        public double CushonWidth { get; set; }
        public double CushonThickness { get; set; }
        public double CushonLength { get; set; }
        public double CushonSpecificWgt { get; set; }
        public double SilicaWidth { get; set; }
        public double SilicaLength { get; set; }
        public double ActualWeight { get; set; }
        public double SilicaWgtM2 { get; set; }
        public double SiliTotalWgt { get; set; }
        double specWgt;
        public double CushThickTol
        {
            get { return 0.00508; }
        }
        public double CushWdtTol
        {
            get { return 1; }
        }
        public double CushLenTol
        {
            get { return 2; }
        }
        public double CushDensTol
        {
            get { return 0.00002388; }
        }

        public double SiliWgtStd
        {
            get { return 0.005; }
        }
        public double SpecWgt
        {
            get
            { return Math.Round(specWgt, 2); }
            set
            { specWgt = (double)value; }
        }
        public double Tolerance { get; set; }
        public double Shella
        {
            get { return 11; }
        }
        public double BarWeight
        {
            get { return 17; }
        }

        DataTable DT = new DataTable();
        DBService dbs = new DBService();
        string StrSql = $@"";
        public Silica()
        {

        }

        public Silica(string item, string num)
        {
            int val;
            Item = item;
            int.TryParse(num, out val);
            Num = val;
        }

        public void GetSilicaData()
        {
            StrSql = $@"SELECT c.ioprod as prod,c.iokod as kod,c.iovi as cushonThk, c.iolen as cusonLen,c.iowdt as cushonWdt ,a.bchld as mix,d.iowdt as silicaWdt, mnwt as specificWgt,d.ioprod as SilicaPrd,d.iolen as SilicaLen,iwght as wghtM2
                        FROM bpcsfali.iimo c 
	                    LEFT JOIN bpcsfv30.mbml01 a on c.ioprod=a.bprod and substring(a.bclac,1,1)='B'
	                    LEFT JOIN bpcsfv30.mbml01 b on c.ioprod=b.bprod and b.bclac='RQ'
	                    LEFT JOIN bpcsfali.iimo d on b.bchld=d.ioprod
	                    LEFT JOIN mfrt.mxnp on a.bchld=mncode and mnmif='F1' and mnlst=' ' 	
                        LEFT JOIN bpcsfv30.iim on iprod=d.ioprod
                        WHERE c.ioprod='FG-{Num}'";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)

            {
                MixNum = DT.Rows[0]["Mix"].ToString();
                CushonWidth = double.Parse(DT.Rows[0]["cushonWdt"].ToString()) * 2.54;
                CushonThickness = double.Parse(DT.Rows[0]["cushonThk"].ToString()) * 2.54;
                CushonLength = double.Parse(DT.Rows[0]["cusonLen"].ToString()) * 100;
                CushonSpecificWgt = double.Parse(DT.Rows[0]["specificWgt"].ToString()) / 1000;
                SilicaLength = double.Parse(DT.Rows[0]["SilicaLen"].ToString()) * 100;
                SilicaWidth = double.Parse(DT.Rows[0]["silicaWdt"].ToString()) * 2.54;
                SilicaWgtM2 = double.Parse(DT.Rows[0]["wghtM2"].ToString());

                var cushonWgt = CushonThickness * CushonWidth * CushonLength * CushonSpecificWgt + Shella + BarWeight; // חישוב משקל הגומי
                SiliTotalWgt = SilicaWidth / 100 * SilicaLength / 100 * SilicaWgtM2; // חישוב משקל הסיליקה

                var cushonErr = Math.Sqrt(Math.Pow(CushonWidth * CushonLength * CushonSpecificWgt, 2) * Math.Pow(CushThickTol, 2) + Math.Pow(CushonThickness * CushonLength * CushonSpecificWgt, 2) * Math.Pow(CushWdtTol, 2) + Math.Pow(CushonSpecificWgt * CushonWidth * CushonThickness, 2) * Math.Pow(CushLenTol, 2) + Math.Pow(CushonThickness * CushonWidth * CushonLength, 2) * Math.Pow(CushDensTol, 2)); // חישוב שגיאת הגומי

                var silicErr = Math.Sqrt(Math.Pow(SilicaWidth / 100 * SilicaLength / 100, 2) * Math.Pow(SiliWgtStd, 2)); // שגיאת הסיליקה
                SpecWgt = cushonWgt + SiliTotalWgt; // משקל גליל צפוי
                Tolerance = Math.Sqrt(Math.Pow(cushonErr, 2) + Math.Pow(silicErr, 2));
            }
        }

        /// <summary>
        /// מחזירה האם המשקל הנמדד של הסיליקה בתוך הטולרנס
        /// </summary>
        /// <returns></returns>
        public bool IsWeightInTolerance()
        {
            if (ActualWeight < SpecWgt - Tolerance || ActualWeight > SpecWgt + Tolerance)
            {
                return false;
            }
            return true;
        }

        public double CalculateDeviation()
        {
            var dev = 1 - (ActualWeight / SpecWgt);
            return Math.Round(dev, 2);
        }

        /// <summary>
        /// הוספת השקילה לבסיס הנתונים
        /// </summary>
        public void AddWeightToDB()
        {
            int lognum = 0;
            DataTable data = new DataTable();
            // קידום מספר הלוג לשמירת הרשומה החדשה
            StrSql = $@"Select  max(LLOGNO) from {Lib_TAPIALI}.LABELGP";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                lognum = (Convert.ToInt32(DT.Rows[0][0].ToString()) + 1);
            }

            // שליפת נתוני עמדה ומכונה
            StrSql = $@"select ICSCP1,RWRKC,mhmach,wdept
                            from bpcsfv30.cicl01
                            left join bpcsfv30.frtl01 on icprod=rprod
                            left join bpcsfv30.lmhl01 on mhwrkc=rwrkc
                            left join bpcsfv30.lwkl01 on wwrkc=rwrkc
                            where icprod='{Item}' and icfac='F1'";
            data = dbs.executeSelectQueryNoParam(StrSql);
            foreach (DataRow row in data.Rows)
            {
                MacID = row["mhmach"].ToString();
                WorkCenter = int.Parse(row["RWRKC"].ToString());
                DepID = int.Parse(row["wdept"].ToString());
                SpecWgt = double.Parse(row["wdept"].ToString());
            }

            var shift = dbs.GetShiftNum();

            StrSql = $@"INSERT INTO {Lib_TAPIALI}.LABELGP(LPROD,LLBLNO,LMACH,LACNDT,LACNTM,LLOGNO,LACTAN,LADESC,LDEPT,LWRKC,LSHIFT,LOVED,LKGMF,LKGAC) 
              VALUES('{Item}',{ID},'{MacID}',{DateTime.Now.ToString("yyyyMMdd")},{DateTime.Now.ToString("HHmmss")},(Select max(LLOGNO) from {Lib_TAPIALI}.LABELGP) +1,300,cast('שקילת סיל' as Character (10) CCSID 424),{DepID},{WorkCenter},{shift},{Form1.WorkerID},{SpecWgt},{ActualWeight - BarWeight - Shella - SiliTotalWgt})";
            dbs.executeInsertQuery(StrSql);

            UpdateMBMG();
        }

        /// <summary>
        /// עדכון הקשר בין פריט אב לפריט בן
        /// </summary>
        public void UpdateMBMG()
        {
            StrSql = $@"INSERT INTO {Bpcs}.MBMG(GPROD,GPLOT,GPQTY,GCHLD,GCLOT,GCQTY,GMACH,GWRKC,GDEPT,GEMP,GDTE,GTIME,GSHFT,GSTMP,GUSER,GWSID)
               VALUES('{Item.PadLeft(21, ' ').Substring(0, 15)}',{Item.PadLeft(21, ' ').Substring(15, 6)}',1,'{MixNum.PadLeft(21, ' ').Substring(0, 15)}',{MixNum.PadLeft(21, ' ').Substring(15, 6)}',{GetMixWeight()},'{MacID}',{WorkCenter},{DepID},{Form1.WorkerID},{DateTime.Now.ToString("yyMMdd")},{DateTime.Now.ToString("HHmmss")},{dbs.GetShiftNum()},{DateTime.Now.ToOADate()},CSHARP,{Environment.MachineName})";
            dbs.executeInsertQuery(StrSql);

        }

        /// <summary>
        /// Gets the Mix weight. If the item is not the correct child item - the value will be -1
        /// </summary>
        /// <returns></returns>
        public double GetMixWeight()
        {
            StrSql = $@"SELECT BQREQ
                        FROM BPCSFV30.MBML01 
                        WHERE BPROD = '{Item.PadLeft(21, ' ').Substring(0, 15)}' AND BCHLD = '{MixNum.PadLeft(21, ' ').Substring(0, 15)}' AND BMWHS = 'F1'";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                return double.Parse(DT.Rows[0]["BQREQ"].ToString());
            }
            return -1;
        }

        /// <summary>
        /// MCOVIP עדכון קובץ 
        /// </summary>
        public void UpdateManufacture()
        {
            // בודקים האם השורה קיימת לפריט הנתון
            StrSql = $@"SELECT OPRIT,ODATE,OSHIFT,OWRKC,OMACH,OEMPT,OSTTS,OADIF,OEMP,OMADE
                        FROM {Rzpali}.MCOVIP
                         WHERE ODATE = { DateTime.Now.ToString("1yyMMdd") } AND ODEPE = {DepID} AND OSHIFT = { dbs.GetShiftNum() } AND OPRIT = '{Item}' AND OMACH = '{MacID}' AND OWRKC = {WorkCenter}";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                // בודקים האם מס העובד הינו אותו עובד אשר מחובר כעת
                if (DT.Rows[0]["OEMP"].ToString() == Form1.WorkerID.ToString())
                {
                    // בודקים האם הסטטוס עבר למלאי
                    if (DT.Rows[0]["OSTTS"].ToString() == "1")
                    {
                        InsertMCOVIP();
                    }
                    else
                    {
                        UpdateMCOVIP(int.Parse(DT.Rows[0]["OMADE"].ToString()));
                    }
                }
                else
                {
                    InsertMCOVIP();
                }
            }
            else
            {
                MessageBox.Show("אין סידור עבודה לפריט הנתון. אנא פנה למנהל המשמרת");
            }
        }

        private void InsertMCOVIP()
        {
            var shift = dbs.GetShiftNum();
            string timeF = "";
            if (shift == 1)
            {
                //new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,)
                TimeSpan T = new TimeSpan(23, 59, 59);
                TimeSpan now = DateTime.Now.TimeOfDay;
                // התאמה לפורמט הזמן של הקובץ
                if (now <= T)
                {
                    timeF = DateTime.Now.ToString("HHmm");
                }
                else
                {
                    timeF = int.Parse(DateTime.Now.ToString("HHmm") + 2400).ToString();
                }
            }
            // עדכון הנומרטור
            StrSql = $@"SELECT MOVL1# FROM {Rzpali}.MOVLP";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            var num = int.Parse(DT.Rows[0]["MOVL1#"].ToString()) + 1;
            StrSql = $@"UPDATE {Rzpali}.MOVLP SET MOVL1#={num}";
            dbs.executeUpdateQuery(StrSql);

            StrSql = $@"INSERT INTO {Rzpali}.MCOVIP(ODATE,ODEPW,OWRKC,OEMP,ODEPE,OMACH,OPRIT,OSHIFT,OPLAN,OMACHT,OMADE,OTIMF,OTIMT,OSTTS,ODTUPD,OUSER,OWSID,OVLNUM) 
                        VALUES({DateTime.Now.ToString("1yyMMdd")},{DepID},{WorkCenter},'000{Form1.WorkerID}',{DepID},'{MacID}','{Item}','{shift}',0,'{MacID}',1,{timeF},{timeF},0,{DateTime.Now.ToString("HHmmssddMMyy")},'CSHARP','{Environment.MachineName}',{num})";
            dbs.executeInsertQuery(StrSql);
        }

        private void UpdateMCOVIP(int omade)
        {
            var shift = dbs.GetShiftNum();
            string timeF = "";
            if (shift == 1)
            {
                if (omade == 0)
                {
                    TimeSpan T = new TimeSpan(23, 59, 59);
                    TimeSpan now = DateTime.Now.TimeOfDay;
                    // התאמה לפורמט הזמן של הקובץ
                    if (now <= T)
                    {
                        timeF = DateTime.Now.ToString("HHmm");
                    }
                    else
                    {
                        timeF = int.Parse(DateTime.Now.ToString("HHmm") + 2400).ToString();
                    }
                }

            }

            StrSql = $@"UPDATE {Rzpali}.MCOVIP
                            SET OMADE = (OMADE + 1)
                            WHERE ODATE = { DateTime.Now.ToString("1yyMMdd") } AND ODEPE = {DepID} AND OSHIFT = { dbs.GetShiftNum() } AND OPRIT = '{Item}' AND OMACH = '{MacID}' AND OWRKC = {WorkCenter}";
            dbs.executeUpdateQuery(StrSql);
        }
    }
}
