﻿namespace Weighting_Silica
{
    partial class Frm_Manage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lbl_Title = new System.Windows.Forms.Label();
            this.Txt_LeftCOM = new System.Windows.Forms.TextBox();
            this.Lbl_LeftCOM = new System.Windows.Forms.Label();
            this.Lbl_RightCOM = new System.Windows.Forms.Label();
            this.Txt_RightCOM = new System.Windows.Forms.TextBox();
            this.Btn_Update = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Lbl_Title
            // 
            this.Lbl_Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.Lbl_Title.Font = new System.Drawing.Font("Arial", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Title.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Lbl_Title.Location = new System.Drawing.Point(0, 0);
            this.Lbl_Title.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Lbl_Title.Name = "Lbl_Title";
            this.Lbl_Title.Size = new System.Drawing.Size(1215, 79);
            this.Lbl_Title.TabIndex = 173;
            this.Lbl_Title.Text = "מסך ניהול";
            this.Lbl_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_LeftCOM
            // 
            this.Txt_LeftCOM.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_LeftCOM.Location = new System.Drawing.Point(199, 101);
            this.Txt_LeftCOM.Name = "Txt_LeftCOM";
            this.Txt_LeftCOM.Size = new System.Drawing.Size(121, 33);
            this.Txt_LeftCOM.TabIndex = 174;
            // 
            // Lbl_LeftCOM
            // 
            this.Lbl_LeftCOM.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Lbl_LeftCOM.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_LeftCOM.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Lbl_LeftCOM.Location = new System.Drawing.Point(25, 103);
            this.Lbl_LeftCOM.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Lbl_LeftCOM.Name = "Lbl_LeftCOM";
            this.Lbl_LeftCOM.Size = new System.Drawing.Size(168, 33);
            this.Lbl_LeftCOM.TabIndex = 175;
            this.Lbl_LeftCOM.Text = "Left COM:";
            this.Lbl_LeftCOM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lbl_RightCOM
            // 
            this.Lbl_RightCOM.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Lbl_RightCOM.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_RightCOM.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Lbl_RightCOM.Location = new System.Drawing.Point(25, 139);
            this.Lbl_RightCOM.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Lbl_RightCOM.Name = "Lbl_RightCOM";
            this.Lbl_RightCOM.Size = new System.Drawing.Size(168, 33);
            this.Lbl_RightCOM.TabIndex = 176;
            this.Lbl_RightCOM.Text = "Right COM:";
            this.Lbl_RightCOM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_RightCOM
            // 
            this.Txt_RightCOM.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_RightCOM.Location = new System.Drawing.Point(199, 141);
            this.Txt_RightCOM.Name = "Txt_RightCOM";
            this.Txt_RightCOM.Size = new System.Drawing.Size(121, 33);
            this.Txt_RightCOM.TabIndex = 177;
            // 
            // Btn_Update
            // 
            this.Btn_Update.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Update.Location = new System.Drawing.Point(545, 560);
            this.Btn_Update.Name = "Btn_Update";
            this.Btn_Update.Size = new System.Drawing.Size(121, 31);
            this.Btn_Update.TabIndex = 178;
            this.Btn_Update.Text = "עדכן";
            this.Btn_Update.UseVisualStyleBackColor = true;
            this.Btn_Update.Click += new System.EventHandler(this.Btn_Update_Click);
            // 
            // Frm_Manage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(1215, 617);
            this.Controls.Add(this.Btn_Update);
            this.Controls.Add(this.Txt_RightCOM);
            this.Controls.Add(this.Lbl_RightCOM);
            this.Controls.Add(this.Lbl_LeftCOM);
            this.Controls.Add(this.Txt_LeftCOM);
            this.Controls.Add(this.Lbl_Title);
            this.Name = "Frm_Manage";
            this.Text = "מסך ניהול";
            this.Load += new System.EventHandler(this.Frm_Manage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Lbl_Title;
        private System.Windows.Forms.TextBox Txt_LeftCOM;
        private System.Windows.Forms.Label Lbl_LeftCOM;
        private System.Windows.Forms.Label Lbl_RightCOM;
        private System.Windows.Forms.TextBox Txt_RightCOM;
        private System.Windows.Forms.Button Btn_Update;
    }
}