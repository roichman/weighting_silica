﻿namespace Weighting_Silica
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Lbl_Title = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.GB_Wgt_R = new System.Windows.Forms.GroupBox();
            this.Txt_ItemR = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_MixR = new System.Windows.Forms.TextBox();
            this.Lbl_ActualWeight = new System.Windows.Forms.Label();
            this.Lbl_Dev = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Srl_Weighting = new System.IO.Ports.SerialPort(this.components);
            this.Gb_LogIn = new System.Windows.Forms.GroupBox();
            this.Txt_WorkerNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_LogIn = new System.Windows.Forms.Button();
            this.Lbl_Worker = new System.Windows.Forms.Label();
            this.Srl_WeightingR = new System.IO.Ports.SerialPort(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.GB_Wgt_R.SuspendLayout();
            this.Gb_LogIn.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1904, 52);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 141;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1904, 24);
            this.menuStrip1.TabIndex = 142;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "יציאה";
            // 
            // Lbl_Title
            // 
            this.Lbl_Title.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Lbl_Title.Font = new System.Drawing.Font("Arial", 51F, System.Drawing.FontStyle.Bold);
            this.Lbl_Title.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Lbl_Title.Location = new System.Drawing.Point(760, 124);
            this.Lbl_Title.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Lbl_Title.Name = "Lbl_Title";
            this.Lbl_Title.Size = new System.Drawing.Size(468, 79);
            this.Lbl_Title.TabIndex = 166;
            this.Lbl_Title.Text = "שקילת סיליקה";
            this.Lbl_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.GB_Wgt_R);
            this.groupBox1.Controls.Add(this.Lbl_ActualWeight);
            this.groupBox1.Controls.Add(this.Lbl_Dev);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox1.Location = new System.Drawing.Point(12, 260);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(1862, 742);
            this.groupBox1.TabIndex = 169;
            this.groupBox1.TabStop = false;
            this.groupBox1.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(385, 606);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 183;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // GB_Wgt_R
            // 
            this.GB_Wgt_R.Controls.Add(this.Txt_ItemR);
            this.GB_Wgt_R.Controls.Add(this.label2);
            this.GB_Wgt_R.Controls.Add(this.label3);
            this.GB_Wgt_R.Controls.Add(this.label4);
            this.GB_Wgt_R.Controls.Add(this.Txt_MixR);
            this.GB_Wgt_R.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.GB_Wgt_R.Location = new System.Drawing.Point(548, 68);
            this.GB_Wgt_R.Name = "GB_Wgt_R";
            this.GB_Wgt_R.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.GB_Wgt_R.Size = new System.Drawing.Size(852, 417);
            this.GB_Wgt_R.TabIndex = 172;
            this.GB_Wgt_R.TabStop = false;
            this.GB_Wgt_R.Text = "משקל ";
            // 
            // Txt_ItemR
            // 
            this.Txt_ItemR.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Bold);
            this.Txt_ItemR.Location = new System.Drawing.Point(102, 313);
            this.Txt_ItemR.MaxLength = 21;
            this.Txt_ItemR.Name = "Txt_ItemR";
            this.Txt_ItemR.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_ItemR.Size = new System.Drawing.Size(649, 65);
            this.Txt_ItemR.TabIndex = 183;
            this.Txt_ItemR.Text = "FG-405";
            this.Txt_ItemR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_ItemR.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txt_ItemR_KeyDown);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.Font = new System.Drawing.Font("Arial", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(217, 38);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(407, 79);
            this.label2.TabIndex = 182;
            this.label2.Text = "פרטי סיליקה";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(354, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 36);
            this.label3.TabIndex = 186;
            this.label3.Text = "תערובת:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(365, 274);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 36);
            this.label4.TabIndex = 184;
            this.label4.Text = "מק\"ט:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Txt_MixR
            // 
            this.Txt_MixR.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_MixR.Location = new System.Drawing.Point(102, 172);
            this.Txt_MixR.MaxLength = 21;
            this.Txt_MixR.Name = "Txt_MixR";
            this.Txt_MixR.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_MixR.Size = new System.Drawing.Size(649, 65);
            this.Txt_MixR.TabIndex = 185;
            this.Txt_MixR.Text = "7255";
            this.Txt_MixR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_MixR.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txt_MixR_KeyDown);
            // 
            // Lbl_ActualWeight
            // 
            this.Lbl_ActualWeight.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Lbl_ActualWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lbl_ActualWeight.Font = new System.Drawing.Font("Arial", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_ActualWeight.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Lbl_ActualWeight.Location = new System.Drawing.Point(979, 554);
            this.Lbl_ActualWeight.Margin = new System.Windows.Forms.Padding(9, 3, 3, 3);
            this.Lbl_ActualWeight.Name = "Lbl_ActualWeight";
            this.Lbl_ActualWeight.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Lbl_ActualWeight.Size = new System.Drawing.Size(250, 130);
            this.Lbl_ActualWeight.TabIndex = 175;
            this.Lbl_ActualWeight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lbl_Dev
            // 
            this.Lbl_Dev.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Lbl_Dev.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lbl_Dev.Font = new System.Drawing.Font("Arial", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Dev.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Lbl_Dev.Location = new System.Drawing.Point(720, 554);
            this.Lbl_Dev.Margin = new System.Windows.Forms.Padding(9, 3, 3, 3);
            this.Lbl_Dev.Name = "Lbl_Dev";
            this.Lbl_Dev.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Lbl_Dev.Size = new System.Drawing.Size(250, 130);
            this.Lbl_Dev.TabIndex = 176;
            this.Lbl_Dev.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(791, 510);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 37);
            this.label6.TabIndex = 173;
            this.label6.Text = "% סטייה:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(1053, 510);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 37);
            this.label7.TabIndex = 174;
            this.label7.Text = "משקל:";
            // 
            // Srl_Weighting
            // 
            this.Srl_Weighting.BaudRate = 2400;
            this.Srl_Weighting.DataBits = 7;
            this.Srl_Weighting.DtrEnable = true;
            this.Srl_Weighting.Parity = System.IO.Ports.Parity.Even;
            this.Srl_Weighting.ReadBufferSize = 1024;
            this.Srl_Weighting.ReadTimeout = 50;
            this.Srl_Weighting.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.Srl_Weighting_DataReceived);
            // 
            // Gb_LogIn
            // 
            this.Gb_LogIn.Controls.Add(this.Txt_WorkerNum);
            this.Gb_LogIn.Controls.Add(this.label1);
            this.Gb_LogIn.Controls.Add(this.Btn_LogIn);
            this.Gb_LogIn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Gb_LogIn.Location = new System.Drawing.Point(12, 143);
            this.Gb_LogIn.Name = "Gb_LogIn";
            this.Gb_LogIn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Gb_LogIn.Size = new System.Drawing.Size(307, 125);
            this.Gb_LogIn.TabIndex = 170;
            this.Gb_LogIn.TabStop = false;
            this.Gb_LogIn.Text = "התחברות";
            // 
            // Txt_WorkerNum
            // 
            this.Txt_WorkerNum.Location = new System.Drawing.Point(39, 33);
            this.Txt_WorkerNum.Name = "Txt_WorkerNum";
            this.Txt_WorkerNum.Size = new System.Drawing.Size(132, 27);
            this.Txt_WorkerNum.TabIndex = 173;
            this.Txt_WorkerNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(187, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 23);
            this.label1.TabIndex = 172;
            this.label1.Text = "מס\' עובד:";
            // 
            // Btn_LogIn
            // 
            this.Btn_LogIn.Location = new System.Drawing.Point(122, 80);
            this.Btn_LogIn.Name = "Btn_LogIn";
            this.Btn_LogIn.Size = new System.Drawing.Size(75, 31);
            this.Btn_LogIn.TabIndex = 0;
            this.Btn_LogIn.Text = "התחבר";
            this.Btn_LogIn.UseVisualStyleBackColor = true;
            this.Btn_LogIn.Click += new System.EventHandler(this.Btn_LogIn_Click);
            // 
            // Lbl_Worker
            // 
            this.Lbl_Worker.AutoSize = true;
            this.Lbl_Worker.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Worker.Location = new System.Drawing.Point(96, 103);
            this.Lbl_Worker.Name = "Lbl_Worker";
            this.Lbl_Worker.Size = new System.Drawing.Size(0, 23);
            this.Lbl_Worker.TabIndex = 171;
            // 
            // Srl_WeightingR
            // 
            this.Srl_WeightingR.BaudRate = 2400;
            this.Srl_WeightingR.DataBits = 7;
            this.Srl_WeightingR.DtrEnable = true;
            this.Srl_WeightingR.Parity = System.IO.Ports.Parity.Even;
            this.Srl_WeightingR.ReadBufferSize = 1024;
            this.Srl_WeightingR.ReadTimeout = 50;
            this.Srl_WeightingR.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.Srl_WeightingR_DataReceived);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(1904, 1042);
            this.Controls.Add(this.Lbl_Worker);
            this.Controls.Add(this.Gb_LogIn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Lbl_Title);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Weighting_Silica";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.GB_Wgt_R.ResumeLayout(false);
            this.GB_Wgt_R.PerformLayout();
            this.Gb_LogIn.ResumeLayout(false);
            this.Gb_LogIn.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label Lbl_Title;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Lbl_ActualWeight;
        private System.Windows.Forms.Label Lbl_Dev;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.IO.Ports.SerialPort Srl_Weighting;
        private System.Windows.Forms.GroupBox Gb_LogIn;
        private System.Windows.Forms.TextBox Txt_WorkerNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_LogIn;
        private System.Windows.Forms.Label Lbl_Worker;
        private System.IO.Ports.SerialPort Srl_WeightingR;
        private System.Windows.Forms.GroupBox GB_Wgt_R;
        private System.Windows.Forms.TextBox Txt_ItemR;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_MixR;
        private System.Windows.Forms.Button button1;
    }
}

